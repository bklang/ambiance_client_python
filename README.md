Ambiance Client for Python

This library implements a Python3 compatible library for interacting with the [Ambiance Server](https://gitlab.com/bklang/ambiance)
